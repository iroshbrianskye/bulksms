from __future__ import print_function

import os
from sqlalchemy import create_engine, String
import app
import pandas as pd
from config import Config, DevelopmentConfig

from flask import (
    Blueprint,
    abort,
    flash,
    redirect,
    render_template,
    send_from_directory,
    current_app
)
import africastalking
from flask_rq import get_queue
from werkzeug.datastructures import MultiDict

from app.email import send_email
from app.models import *
from app.decorators import admin_required
from flask_login import current_user, login_required
from app.admin.forms import *
from app.auth.forms import *
from app.auth.admin_decorators import check_confirmed

admin = Blueprint("admin", __name__)
username = "kutrrh-info"
api_key = "e3f86443ee26136015c6e79727613eff6be33cd499dc8be3ba36df1685d1e18c"
africastalking.initialize(username, api_key)
db_URI = DevelopmentConfig.SQLALCHEMY_DATABASE_URI
engine = create_engine(db_URI)

sms = africastalking.SMS


@admin.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


@admin.route("/")
@login_required
@admin_required
@check_confirmed
def dashboard():
    """Admin dashboard page."""
    messagesCount = Message.query.count()
    messagesCountToday = Message.query.order_by(Message.createdAt.desc()).limit(5).count()

    return render_template(
        "admin/index.html", messagesCount=messagesCount, messagesCountToday=messagesCountToday)


@admin.route("/send_messages", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def send_messages():
    all_groups = Group.query.order_by(Group.createdAt.desc()).all()
    form = MessageForm()
    form2 = SelectGroupForm()
    if form.validate_on_submit():
        sms_message = request.form['sms_message']
        phone_number = request.form['phone_number']
        sender = "KUTRRH-INFO"

        pn = phone_number.split(',')
        pnn = ["+" + x.strip(' ') for x in pn]
        print(pnn)

        try:
            # That's it, hit send and we'll take care of the rest.
            response = sms.send(sms_message, pnn, sender)
            flash("SMS sent successfully", "success")
            print(response)
            my_values = response["SMSMessageData"]["Recipients"]
            for mv in my_values:
                print(mv["number"])
                new_message = Message(
                    sender_id=current_user.id,
                    sms_message=form.sms_message.data,
                    phone_number=mv["number"],
                    status=mv["status"]
                )
                db.session.add(new_message)
                db.session.commit()
        except Exception as e:
            print('Encountered an error while sending: %s' % str(e))
            flash("SMS not sent successfully", "danger")

        # new_message = Message(
        #     sender_id=current_user.id,
        #     sms_message=form.sms_message.data,
        # )
        # db.session.add(new_message)
        # db.session.commit()
        return redirect(url_for("admin.dashboard"))
    if form2.validate_on_submit():
        selected_group = form2.group.data
        return redirect(url_for("admin.send_messages_to_group", group_id=selected_group.group_id))
    return render_template('admin/send_messages.html', form=form, all_groups=all_groups, form2=form2)


@admin.route("/send_messages_to_group/<group_id>", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def send_messages_to_group(group_id):
    group = Group.query.filter_by(group_id=group_id).first()
    form = MessageGroupForm()
    pn = []
    for x in group.patients:
        pn.append("+254" + x.phone_number)
    if form.validate_on_submit():
        sms_message = request.form['sms_message']
        sender = "KUTRRH"
        try:
            # That's it, hit send and we'll take care of the rest.
            response = sms.send(sms_message, pn, sender)
            flash("SMS sent successfully", "success")
            print(response)
            new_message = Message(
                sender_id=current_user.id,
                sms_message=form.sms_message.data,
            )
            db.session.add(new_message)
            db.session.commit()
        except Exception as e:
            print('Encountered an error while sending: %s' % str(e))
            flash("SMS not sent successfully", "danger")

        return redirect(url_for("admin.dashboard"))
    return render_template('admin/send_messages_to_group.html', group=group, form=form)


@admin.route("/send_messages_to_all_staff/send", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def send_messages_to_all_staff():
    all_staff = Staff.query.all()
    form = MessageGroupForm()
    pn = []
    for x in all_staff:
        pn.append("+" + x.phone_number)
    if form.validate_on_submit():
        sms_message = request.form['sms_message']
        sender = "KUTRRH"
        try:
            # That's it, hit send and we'll take care of the rest.
            response = sms.send(sms_message, pn, sender)
            flash("SMS sent successfully", "success")
            print(response)
            new_message = Message(
                sender_id=current_user.id,
                sms_message=form.sms_message.data,
            )
            db.session.add(new_message)
            db.session.commit()
        except Exception as e:
            print('Encountered an error while sending: %s' % str(e))
            flash("SMS not sent successfully", "danger")

        return redirect(url_for("admin.dashboard"))
    return render_template('admin/send_messages_to_staff.html', group=group, form=form)


@admin.route("/sent_messages", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def view_messages():
    all_messages = Message.query.order_by(Message.createdAt.desc()).all()
    return render_template('admin/view_messages.html', all_messages=all_messages)


@admin.route("/all_groups", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def view_all_groups():
    all_groups = Group.query.order_by(Group.createdAt.desc()).all()

    return render_template('admin/all_groups.html', all_groups=all_groups)


@admin.route("/add_new_group", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def new_group():
    """Create a new group."""
    form = NewGroupForm()
    if form.validate_on_submit():
        group = Group(
            name=form.name.data
        )
        db.session.add(group)
        db.session.commit()
        flash("Group {} successfully created".format(group.name), "success")
        return redirect(url_for('admin.view_all_groups'))
    return render_template("admin/new_group.html", form=form)


@admin.route("/group/<int:group_id>/_delete")
@login_required
@admin_required
def delete_group(group_id):
    """Delete a group."""
    group = Group.query.filter_by(group_id=group_id).first()
    db.session.delete(group)
    db.session.commit()
    flash("Successfully deleted group %s." % group.name, "success")
    return redirect(url_for("admin.view_all_groups"))


@admin.route("/group/<int:group_id>/view_a")
@login_required
@admin_required
def view_group_staff(group_id):
    """View a group."""
    group = Group.query.filter_by(group_id=group_id).first()

    return render_template("admin/view_group_staff.html", group=group)


@admin.route("/group/<int:group_id>/view_b")
@login_required
@admin_required
def view_group_patients(group_id):
    """View a group."""
    group = Group.query.filter_by(group_id=group_id).first()

    return render_template("admin/view_group_patients.html", group=group)


@admin.route("/group/<int:group_id>/add_staff")
@login_required
@admin_required
def add_staff_to_group(group_id):
    """Add users to a group."""
    group = Group.query.filter_by(group_id=group_id).first()
    all_staff = Staff.query.order_by(Staff.createdAt.desc()).all()

    return render_template("admin/add_users_to_group.html", group=group, all_staff=all_staff)


@admin.route("/group/<int:group_id>/add_staff_submit", methods=["POST"])
@login_required
@admin_required
def add_staff_submit(group_id):
    """Add users to a group."""
    group = Group.query.filter_by(group_id=group_id).first()
    email = request.args.get("emails", 0, str)
    staff = Staff.query.filter_by(email=email).first()

    s = staff
    g = group
    g.staff.append(s)
    db.session.add(g)
    db.session.commit()
    flash("Successfully added staff to group %s." % group.name, "success")
    return redirect(url_for("admin.view_all_groups"))


@admin.route("/group/<int:group_id>/add_patients")
@login_required
@admin_required
def add_patient_to_group(group_id):
    """Add users to a group."""
    group = Group.query.filter_by(group_id=group_id).first()
    all_patients = Patient.query.order_by(Patient.createdAt.desc()).all()

    return render_template("admin/add_patient_to_group.html", group=group, all_patients=all_patients)


@admin.route("/group/<int:group_id>/add_patient_submit", methods=["POST"])
@login_required
@admin_required
def add_patient_submit(group_id):
    """Add users to a group."""
    group = Group.query.filter_by(group_id=group_id).first()
    name = request.args.get("names", 0, str)
    patient = Patient.query.filter_by(name=name).first()

    p = patient
    g = group
    g.patients.append(p)
    db.session.add(g)
    db.session.commit()
    flash("Successfully added patients to group %s." % group.name, "success")
    return redirect(url_for("admin.view_all_groups"))


@admin.route("/all_staff", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def view_all_staff():
    all_staff = Staff.query.order_by(Staff.createdAt.desc()).all()
    return render_template('admin/all_staff.html', all_staff=all_staff)


@admin.route("/add_new_staff", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def new_staff():
    """Create a new staff member."""
    form = NewStaffForm()
    if form.validate_on_submit():
        staff = Staff(
            name=form.name.data,
            email=form.email.data,
            phone_number=form.phone_number.data,
            directorate=form.directorate.data,
            department=form.department.data
        )
        db.session.add(staff)
        db.session.commit()
        flash("Staff member {} successfully created".format(staff.name), "success")
        return redirect(url_for('admin.view_all_staff'))
    return render_template("admin/new_staff.html", form=form)


@admin.route("/import_staff", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def upload_staff_csv():
    if request.method == 'POST':
        xls_file = request.files['file']
        staff_file = pd.read_excel(xls_file)
        new_columns = [column.replace(' ', '_').lower() for column in staff_file]
        staff_file.columns = new_columns
        staff_file.to_sql("staff",
                          engine,
                          if_exists='append',
                          schema='bulksms',
                          index=False,
                          chunksize=500,
                          dtype={"name": String,
                                 "email": String,
                                 "phone_number": String,
                                 "directorate": String,
                                 "department": String
                                 })
        flash("Staff members successfully imported", "success")
        return redirect(url_for('admin.view_all_staff'))
    return render_template('admin/import_new_staff.html')


@admin.route("/all_patients", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def view_all_patients():
    all_patients = Patient.query.order_by(Patient.createdAt.desc()).all()
    return render_template('admin/all_patients.html', all_patients=all_patients)


@admin.route("/add_new_patient", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def new_patient():
    """Create a new patient."""
    form = NewPatientForm()
    if form.validate_on_submit():
        patient = Patient(
            name=form.name.data,
            email=form.email.data,
            phone_number=form.phone_number.data,
            status=form.status.data,
            ward=form.ward.data
        )
        db.session.add(patient)
        db.session.commit()
        flash("Patient {} successfully created".format(patient.name), "success")
        return redirect(url_for('admin.view_all_patients'))
    return render_template("admin/new_patient.html", form=form)


@admin.route("/import_patients", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def upload_patients_xls():
    if request.method == 'POST':
        xls_file = request.files['file']
        patients_file = pd.read_excel(xls_file)
        new_columns = [column.replace(' ', '_').lower() for column in patients_file]
        patients_file.columns = new_columns
        patients_file.to_sql("patients",
                             engine,
                             if_exists='append',
                             schema='bulksms',
                             index=False,
                             chunksize=500,
                             dtype={"name": String,
                                    "email": String,
                                    "phone_number": String,
                                    "status": String,
                                    "ward": String
                                    })
        flash("Patients successfully imported", "success")
        return redirect(url_for('admin.view_all_patients'))
    return render_template('admin/import_new_patients.html')


@admin.route("/add_new_user", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def new_user():
    """Create a new user."""
    form = NewUserForm()
    if form.validate_on_submit():
        user = User(
            role=form.role.data,
            first_name=form.first_name.data,
            second_name=form.second_name.data,
            email=form.email.data,
            password=form.password.data
        )
        db.session.add(user)
        db.session.commit()
        flash("User {} successfully created".format(user.full_name()), "success")
    return render_template("admin/new_user.html", form=form)


@admin.route("/invite-user", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def invite_user():
    """Invites a new user to create an account and set their own password."""
    form = InviteUserForm()
    if form.validate_on_submit():
        user = User(
            role=form.role.data,
            first_name=form.first_name.data,
            second_name=form.second_name.data,
            email=form.email.data,
        )
        db.session.add(user)
        db.session.commit()
        token = user.generate_confirmation_token()
        invite_link = url_for(
            "account.join_from_invite", user_id=user.id, token=token, _external=True
        )
        get_queue().enqueue(
            send_email,
            recipient=user.email,
            subject="You Are Invited To Join",
            template="account/email/invite",
            user=user,
            invite_link=invite_link,
        )
        flash("User {} successfully invited".format(user.full_name()), "form-success")
    return render_template("admin/invite_user.html", form=form)


@admin.route("/all_users")
@login_required
@admin_required
@check_confirmed
def registered_users():
    """View all registered users."""
    users = User.query.all()
    roles = Role.query.all()
    return render_template("admin/registered_users.html", users=users, roles=roles)


@admin.route("/view_user/<int:user_id>")
@admin.route("/view_user/<int:user_id>/info")
@login_required
@admin_required
@check_confirmed
def user_info(user_id):
    """View a user's profile."""
    user = User.query.filter_by(id=user_id).first()

    messagesCount = Message.query.filter_by(sender_id=user_id).count()
    if user is None:
        abort(404)
    return render_template(
        "admin/manage_user.html",
        user=user,
        messagesCount=messagesCount
    )


@admin.route("/user/<int:user_id>/change-email", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def change_user_email(user_id):
    """Change a user's email."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    form = ChangeUserEmailForm()
    if form.validate_on_submit():
        user.email = form.email.data
        db.session.add(user)
        db.session.commit()
        flash(
            "Email for user {} successfully changed to {}.".format(
                user.full_name(), user.email
            ),
            "form-success",
        )
    return render_template("admin/manage_user.html", user=user, form=form)


@admin.route("/user/<int:user_id>/change-account-type", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def change_account_type(user_id):
    """Change a user's account type."""
    if current_user.id == user_id:
        flash(
            "You cannot change the type of your own account. Please ask "
            "another administrator to do this.",
            "error",
        )
        return redirect(url_for("admin.user_info", user_id=user_id))

    user = User.query.get(user_id)
    if user is None:
        abort(404)
    form = ChangeAccountTypeForm()
    if form.validate_on_submit():
        user.role = form.role.data
        db.session.add(user)
        db.session.commit()
        flash(
            "Role for user {} successfully changed to {}.".format(
                user.full_name(), user.role.name
            ),
            "form-success",
        )
    return render_template("admin/manage_user.html", user=user, form=form)


@admin.route("/user/<int:user_id>/delete")
@login_required
@admin_required
@check_confirmed
def delete_user_request(user_id):
    """Request deletion of a user's account."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    return render_template("admin/manage_user.html", user=user)


@admin.route("/user/<int:user_id>/_delete")
@login_required
@admin_required
def delete_user(user_id):
    """Delete a user's account."""
    if current_user.id == user_id:
        flash(
            "You cannot delete your own account. Please ask another "
            "administrator to do this.",
            "error",
        )
    else:
        user = User.query.filter_by(id=user_id).first()
        db.session.delete(user)
        db.session.commit()
        flash("Successfully deleted user %s." % user.full_name(), "success")
    return redirect(url_for("admin.registered_users"))


@admin.route("/<int:user_id>/_suspend/<sender>")
@login_required
@admin_required
@check_confirmed
def suspend(user_id, sender):
    """Suspend a user's account."""
    user = User.query.filter_by(id=user_id).first()
    if current_user.id == user_id:
        flash(
            "You cannot suspend your own account. Please ask another "
            "administrator to do this.",
            "error",
        )
    else:
        if user.status == 1:
            user.status = 0
            if user.role.index == "sender":
                for listing in user.listings:
                    listing.status = 0
                    listing.published = 0
        else:
            user.status = 1
        db.session.commit()
        flash("Successfully suspended user %s." % user.full_name(), "success")
    if sender == "sender":
        return redirect(url_for("admin.senders"))
    else:
        return redirect(url_for("admin.applicants"))


@admin.route("/settings", methods=("GET", "POST"))
@login_required
@admin_required
def admin_settings():
    user = current_user

    return render_template("admin/settings.html", user=user)


@admin.route("/settings/change_password", methods=("GET", "POST"))
@login_required
@admin_required
@check_confirmed
def change_password():
    """Change an existing user's password."""
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.new_password.data
            db.session.add(current_user)
            db.session.commit()
            flash("Your password has been updated.", "success")
            return redirect(url_for("admin.settings"))
        else:
            flash("Original password is invalid.", "red")
    return render_template("admin/change_password.html", form=form)


@admin.route("/settings/change-email", methods=["GET", "POST"])
@admin_required
@login_required
@check_confirmed
def change_email_request():
    """Respond to existing user's request to change their email."""
    form = ChangeEmailForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.password.data):
            new_email = form.email.data
            token = current_user.generate_email_change_token(new_email)
            change_email_link = url_for(
                "account.change_email", token=token, _external=True
            )
            get_queue().enqueue(
                send_email,
                recipient=new_email,
                subject="Confirm Your New Email",
                template="account/email/change_email",
                # current_user is a LocalProxy, we want the underlying user
                # object
                user=current_user._get_current_object(),
                change_email_link=change_email_link,
            )
            flash(
                "A confirmation link has been sent to {}.".format(new_email), "warning"
            )
            return redirect(url_for("admin.settings"))
        else:
            flash("Invalid email or password.", "form-error")
    return render_template("admin/change_email.html", form=form)


@admin.route("/settings/change-email/<token>", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def change_email(token):
    """Change existing user's email with provided token."""
    if current_user.change_email(token):
        flash("Your email address has been updated.", "success")
    else:
        flash("The confirmation link is invalid or has expired.", "error")
    return redirect(url_for("admin.dashboard"))
