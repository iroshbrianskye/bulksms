from flask_wtf import Form, FlaskForm
from wtforms import ValidationError
from flask_ckeditor import CKEditorField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.fields import (
    PasswordField,
    StringField,
    SubmitField,
    SelectField,
    TextAreaField,
    FileField,
    DateField,
    IntegerField,
    FieldList,
    FormField, BooleanField)
from wtforms.fields.html5 import EmailField
from wtforms.validators import Email, EqualTo, InputRequired, Length, DataRequired
from flask_uploads import UploadSet, configure_uploads, IMAGES
from flask_wtf.file import FileField, FileRequired, FileAllowed
from app import db
from app.models import Role, User, Group

photos = UploadSet("photos", IMAGES)


def get_pk(obj):
    return str(obj)


class ChangeUserEmailForm(FlaskForm):
    email = EmailField(
        "New email", validators=[InputRequired(), Length(1, 64), Email()]
    )
    submit = SubmitField("Update email")

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError("Email already registered.")


class ChangeAccountTypeForm(FlaskForm):
    role = QuerySelectField(
        'New account type',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Role).order_by('permissions'), get_pk=get_pk)
    submit = SubmitField('Update role')


class InviteUserForm(FlaskForm):
    role = QuerySelectField(
        'Account type',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Role).order_by('permissions'), get_pk=get_pk)
    first_name = StringField("First name", validators=[InputRequired(), Length(1, 64)])
    second_name = StringField("Last name", validators=[InputRequired(), Length(1, 64)])
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    submit = SubmitField("Invite")

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError("Email already registered.")


class NewUserForm(InviteUserForm):
    password = PasswordField("Password", validators=[InputRequired()])
    password2 = PasswordField(
        "Confirm password",
        validators=[InputRequired(), EqualTo("password", "Passwords must match.")],
    )
    confirmed = BooleanField("", default=True)

    submit = SubmitField("Create")


class MessageForm(FlaskForm):
    sms_message = TextAreaField("SMS Messages", validators=[InputRequired()])
    phone_number = StringField("Phone Numbers", validators=[InputRequired()])
    submit = SubmitField("Send Message")


class SelectGroupForm(FlaskForm):
    group = QuerySelectField(
        'Group',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Group).order_by(Group.createdAt.desc()), get_pk=get_pk)
    submit = SubmitField("Select Group")


class MessageGroupForm(FlaskForm):
    sms_message = TextAreaField("SMS Messages", validators=[InputRequired()])
    phone_number = StringField("Phone Numbers")
    submit = SubmitField("Send Message")


class DisclaimerForm(FlaskForm):
    text = StringField("Text")
    submit = SubmitField("Save")


class NewGroupForm(FlaskForm):
    name = StringField("Name", validators=[InputRequired()])
    submit = SubmitField("Add")


class NewStaffForm(FlaskForm):
    name = StringField("Name", validators=[InputRequired()])
    email = StringField("Email", validators=[InputRequired()])
    phone_number = StringField("Phone Number", validators=[InputRequired()])
    directorate = StringField("Directorate", validators=[InputRequired()])
    department = StringField("Department", validators=[InputRequired()])
    submit = SubmitField("Add")


class NewPatientForm(FlaskForm):
    name = StringField("Name", validators=[InputRequired()])
    email = StringField("Email")
    phone_number = StringField("Phone Number", validators=[InputRequired()])
    status = SelectField(
        validators=[DataRequired()], choices=[("confirmed", "CONFIRMED"), ("suspected", "SUSPECTED"),
                                              ("negative", "NEGATIVE")]
    )
    ward = StringField("Ward", validators=[InputRequired()])
    submit = SubmitField("Add")



