"""
These imports enable us to make all defined models members of the models
module (as opposed to just their python files)
"""

from .users import *
from .messages import *
from .roles import *
from .staff import *
from .patients import *
from .patients_covid_confirmed import *
from .patients_covid_suspected import *
from .senders import *
from .group import *
