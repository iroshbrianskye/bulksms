from datetime import datetime

from flask import current_app

from .. import db
from app.models.group import Group, group_staff


class Staff(db.Model):
    __tablename__ = 'staff'
    staff_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(2000), index=True)
    email = db.Column(db.String(2000), index=True)
    phone_number = db.Column(db.String(64), index=True)
    directorate = db.Column(db.String(64), index=True)
    department = db.Column(db.String(64), index=True)
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Staff {}>'.format(self.name)

    def get_count(self):
        data = Group.query.join(
            group_staff, (group_staff.c.group_id == Group.id)).filter(
            group_staff.c.category_id == self.id)
        return data.count()


def get_staff(data):
    list = []
    for field in [(row.name) for row in Staff.query.all()]:
        if field in data:
            list.append(int(data[field]))
    groups = Group.query.order_by(Group.createdAt.desc()).all()
    if list:
        groups = Group.query.join(
            group_staff, (group_staff.c.group_id == Group.id)).filter(
                group_staff.c.staff_id.in_(list)).order_by(Group.createdAt.desc()).all()
    return groups
