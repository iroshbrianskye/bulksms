from datetime import datetime

from .. import db


class Message(db.Model):
    __tablename__ = 'messages'
    id = db.Column(db.Integer, primary_key=True)
    sender_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
    sms_message = db.Column(db.String(2000), index=True)
    phone_number = db.Column(db.Text)
    status = db.Column(db.String(2000))

    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Message {}>'.format(self.sms_message)

    def get_time(self):
        return self.createdAt


