from datetime import datetime

from .. import db

group_staff = db.Table('group_staff',
                       db.Column('group_id', db.Integer, db.ForeignKey('groups.group_id')),
                       db.Column('staff_id', db.Integer, db.ForeignKey('staff.staff_id')))

group_patient = db.Table('group_patient',
                         db.Column('group_id', db.Integer, db.ForeignKey('groups.group_id')),
                         db.Column('patient_id', db.Integer, db.ForeignKey('patients.patient_id')))


class Group(db.Model):
    __tablename__ = 'groups'
    group_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), index=True)
    users = db.relationship('User', backref='group', lazy='dynamic')
    staff = db.relationship('Staff', secondary=group_staff, backref='group', lazy='dynamic')
    patients = db.relationship('Patient', secondary=group_patient, backref='group', lazy='dynamic')
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Group {}>'.format(self.name)

    def all_staff(self):
        return self.staff.all()

    def has_staff(self):
        return len(self.staff.all()[:1]) == 1

    def has_patients(self):
        return len(self.staff.all()[:1]) == 1
