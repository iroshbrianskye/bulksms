from datetime import datetime

from .. import db


class Patient(db.Model):
    __tablename__ = 'patients'
    patient_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(2000), index=True)
    email = db.Column(db.String(2000), index=True)
    phone_number = db.Column(db.String(64), index=True)
    status = db.Column(db.String(64), index=True)
    ward = db.Column(db.String(64), index=True)
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Patient {}>'.format(self.name)
