from .. import db
from datetime import datetime
from flask import current_app
from app.models.roles import Role


class Sender(db.Model):
    __tablename__ = "senders"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120))
    user_id = db.Column(db.Integer(), db.ForeignKey("users.id"))
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(
        db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow
    )

    def __repr__(self):
        return '<Sender \'%s\'>' % self.title

