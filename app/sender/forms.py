from flask import url_for
from flask_wtf import Form, FlaskForm
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms import ValidationError
from wtforms.fields import (
    BooleanField,
    PasswordField,
    StringField,
    SubmitField,
    TextField,
    DateField,
    DecimalField,
    SelectField,
    TextAreaField,
    IntegerField,
    FileField)
from wtforms import RadioField
from wtforms.fields.html5 import EmailField
from wtforms.validators import Email, EqualTo, InputRequired, Length, DataRequired
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, configure_uploads, DOCUMENTS, IMAGES, TEXT
from flask_login import current_user
from app.models import User
from app.models import *

certificates = UploadSet("certificates", TEXT + DOCUMENTS + IMAGES)


def get_pk(obj):
    return str(obj)


class ReviewForm(FlaskForm):
    stars = RadioField('Label', coerce=int)
    comment = TextAreaField(
        'Comment', validators=[InputRequired(),
                               Length(1, 300)])
    submit = SubmitField('Submit')

#
# class MessageForm(Form):
#     message = TextAreaField(
#         "Message", validators=[DataRequired(), Length(min=0, max=140)]
#     )
#     submit = SubmitField("Submit")


class ProfileForm(FlaskForm):
    first_name = StringField(
        'First name', validators=[InputRequired(),
                                  Length(1, 64)])
    surname = StringField(
        'Surname', validators=[InputRequired(),
                               Length(1, 64)])
    email = EmailField(
        'Email', validators=[InputRequired(),
                             Length(1, 64),
                             Email()])
    address = StringField('Address')
    dob = DateField('Date Of Birth', validators=[DataRequired()], format='%d/%m/%Y')

    submit = SubmitField('Submit')


class PersonalDetailsForm(FlaskForm):
    title = StringField(
        'Title')
    first_name = StringField(
        'First name', validators=[InputRequired(),
                                  Length(1, 64)])
    surname = StringField(
        'Surname', validators=[InputRequired(),
                               Length(1, 64)])
    second_name = StringField(
        'Second name', validators=[InputRequired(),
                                   Length(1, 64)])
    gender = SelectField(validators=[DataRequired()],
                         choices=[('male', 'MALE'), ('female', 'FEMALE')])
    pob = StringField(
        'Place Of Birth', validators=[InputRequired(),
                                      Length(1, 100)])
    dob = DateField('Date Of Birth', validators=[DataRequired()], format='%d/%m/%Y')

    civil_status = SelectField(validators=[DataRequired()],
                               choices=[('cohabitation', 'Cohabitation'), ('divorced', 'Divorced'),
                                        ('domestic_partner', 'Domestic Partner'),
                                        ('legally_separated', 'Legally Separated'), ('married', 'Married'),
                                        ('separated', 'Separated'), ('single', 'Single'),
                                        ('undetermined', 'Undetermined')])
    nationality = StringField(
        'Nationality', validators=[InputRequired(),
                                   Length(1, 64)])
    disability = SelectField(
                             choices=[('yes', 'YES'), ('no', 'NO')])
    disability_nature = StringField(
        'Disability Nature')
    disability_registration_details = StringField(
        'Disability Registration Details')

    submit = SubmitField('Submit')


class ContactInfoForm(FlaskForm):
    email = EmailField(
        'Email', validators=[InputRequired(),
                             Length(1, 64),
                             Email()])
    address = StringField(
        'Address', validators=[InputRequired(),
                               Length(1, 64)])
    county = StringField(
        'County', validators=[InputRequired(),
                              Length(1, 64)])
    phone_number_personal = StringField(
        'Mobile Number', validators=[InputRequired(),
                                     Length(1, 64)])
    phone_number_alternate = StringField('Alternate Number')
    submit = SubmitField('Save')

class MessageForm(FlaskForm):
    sms_message = TextAreaField("SMS Messages", validators=[InputRequired()])
    phone_number = TextAreaField("Phone Numbers", validators=[InputRequired()])
    submit = SubmitField("Send Message")


class SelectGroupForm(FlaskForm):
    group = QuerySelectField(
        'Group',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Group).order_by(Group.createdAt.desc()), get_pk=get_pk)
    submit = SubmitField("Select Group")


class MessageGroupForm(FlaskForm):
    sms_message = TextAreaField("SMS Messages", validators=[InputRequired()])
    phone_number = StringField("Phone Numbers")
    submit = SubmitField("Send Message")


class DisclaimerForm(FlaskForm):
    text = StringField("Text")
    submit = SubmitField("Save")


class NewGroupForm(FlaskForm):
    name = StringField("Name", validators=[InputRequired()])
    submit = SubmitField("Add")


class NewStaffForm(FlaskForm):
    name = StringField("Name", validators=[InputRequired()])
    email = StringField("Email", validators=[InputRequired()])
    phone_number = StringField("Phone Number", validators=[InputRequired()])
    directorate = StringField("Directorate", validators=[InputRequired()])
    department = StringField("Department", validators=[InputRequired()])
    submit = SubmitField("Add")


class NewPatientForm(FlaskForm):
    name = StringField("Name", validators=[InputRequired()])
    email = StringField("Email")
    phone_number = StringField("Phone Number", validators=[InputRequired()])
    status = SelectField(
        validators=[DataRequired()], choices=[("confirmed", "CONFIRMED"), ("suspected", "SUSPECTED"),
                                              ("negative", "NEGATIVE")]
    )
    ward = StringField("Ward", validators=[InputRequired()])
    submit = SubmitField("Add")