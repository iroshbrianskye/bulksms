from flask import (
    Blueprint,
    render_template,
    redirect,
    url_for,
    jsonify,
    flash,
    send_from_directory,
)
from flask_ckeditor import upload_success
from flask_login import (
    current_user,
    login_required,
)
import PIL, simplejson, traceback
from PIL import Image
from flask_rq import get_queue
from sqlalchemy import create_engine, String
from app.models.users import *
from app.sender.forms import *
from app.auth.forms import *
from app import db
from app.email import send_email
from app.models import User, Message
from app.auth.email import send_password_reset_email
from app.auth.admin_decorators import check_confirmed
from werkzeug import secure_filename
from app.lib.upload_file import uploadfile
from werkzeug.datastructures import FileStorage
import africastalking

from config import DevelopmentConfig

ALLOWED_EXTENSIONS = {"txt", "gif", "png", "jpg", "jpeg", "bmp", "rar", "zip", "7zip", "doc", "docx", "pdf"}

sender = Blueprint('sender', __name__)
username = "kutrrh-info"
api_key = "266144f52e14d1f7160ade314da1fa048aec186e9bd26e54dfd15134fa5e89f5"
africastalking.initialize(username, api_key)
db_URI = DevelopmentConfig.SQLALCHEMY_DATABASE_URI
engine = create_engine(db_URI)

sms = africastalking.SMS


@sender.route('/')
@login_required
@check_confirmed
def dashboard():
    """Admin dashboard page."""

    messagesCount = Message.query.count()
    messagesCountToday = Message.query.order_by(Message.createdAt.desc()).limit(5).count()

    return render_template(
        "sender/index.html", messagesCount=messagesCount, messagesCountToday=messagesCountToday)


@sender.route('/send_messages', methods=['post', 'get'])
@login_required
@check_confirmed
def send_messages():
    all_groups = Group.query.order_by(Group.createdAt.desc()).all()
    form = MessageForm()
    form2 = SelectGroupForm()
    if form.validate_on_submit():
        sms_message = request.form['sms_message']
        phone_number = request.form['phone_number']
        sender = "KUTRRH-INFO"

        pn = phone_number.split(',')
        pnn = ["+" + x.strip(' ') for x in pn]
        print(pnn)

        try:
            # That's it, hit send and we'll take care of the rest.
            response = sms.send(sms_message, pnn, sender)
            flash("SMS sent successfully", "success")
            print(response)
            my_values = response["SMSMessageData"]["Recipients"]
            for mv in my_values:
                new_message = Message(
                    sender_id=current_user.id,
                    sms_message=form.sms_message.data,
                    phone_number=mv["number"],
                    status=mv["status"]
                )
                db.session.add(new_message)
                db.session.commit()
        except Exception as e:
            print('Encountered an error while sending: %s' % str(e))
            flash("SMS not sent successfully", "danger")

        return redirect(url_for("sender.dashboard"))
    if form2.validate_on_submit():
        selected_group = form2.group.data
        return redirect(url_for("admin.send_messages_to_group", group_id=selected_group.group_id))
    return render_template('sender/send_messages.html', form=form, all_groups=all_groups, form2=form2)


@sender.route('/profile', methods=['post', 'get'])
@login_required
@check_confirmed
def profile():
    """Admin dashboard page."""

    form = ProfileForm(obj=current_user)

    if form.validate_on_submit():
        form.populate_obj(current_user)
        db.session.commit()
        return redirect(url_for('sender.profile'))
    return render_template('sender/user-profile-page.html', form=form)


@sender.route('/settings', methods=['post', 'get'])
@login_required
@check_confirmed
def settings():
    """Admin dashboard page."""

    if current_user.is_anonymous:
        return redirect(url_for('home.index'))
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.new_password.data
            db.session.add(current_user)
            db.session.commit()
            flash('Your password has been updated.', "success")
            return redirect(url_for('sender.dashboard'))
        else:
            flash('Original password is invalid.', "warning")
    return render_template('account/reset_password.html', form=form)


@sender.route("/sent_messages")
@login_required
@check_confirmed
def sent_messages():
    all_messages = Message.query.order_by(Message.createdAt.desc()).all()
    return render_template('sender/view_messages.html', all_messages=all_messages)


@sender.route("/files/<path:filename>")
def uploaded_files(filename):
    path = current_app.config["UPLOADS"]
    return send_from_directory(path, filename)


@sender.route("/upload", methods=["POST"])
def upload():
    f = request.files.get("file")
    # Add more validations here
    extension = f.filename.split(".")[1].lower()

    f.save(os.path.join("app/static/uploads", f.filename))
    url = url_for("admin.uploaded_files", filename=f.filename)
    return upload_success(url=url)  # return upload_success call


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


def gen_file_name(filename):
    """
    If file was exist already, rename it and return a new name
    """

    i = 1
    while os.path.exists(os.path.join(current_app.config["UPLOAD_FOLDER"], filename)):
        name, extension = os.path.splitext(filename)
        filename = "%s_%s%s" % (name, str(i), extension)
        i += 1

    return filename


def create_thumbnail(image):
    try:
        base_width = 100
        img = Image.open(os.path.join(current_app.config["UPLOAD_FOLDER"], image))
        w_percent = base_width / float(img.size[0])
        h_size = int((float(img.size[1]) * float(w_percent)))
        img = img.resize((base_width, h_size), PIL.Image.ANTIALIAS)
        img.save(os.path.join(current_app.config["THUMBNAIL_FOLDER"], image))

        return True

    except:
        print(traceback.format_exc())
        return False


@sender.route("/upload/<p_table>/<c_table>/<id>", methods=["GET", "POST"])
def upload_image(p_table, c_table, id):
    if request.method == "POST":
        files = request.files["file"]

        if files:
            filename = secure_filename(files.filename)
            filename = gen_file_name(filename)
            mime_type = files.content_type
            p_model = User.get_class_by_tablename(p_table)

            if not allowed_file(files.filename):
                result = uploadfile(
                    name=filename,
                    table=c_table,
                    type=mime_type,
                    size=0,
                    not_allowed_msg="File type not allowed",
                )

            else:
                # save file to disk
                uploaded_file_path = os.path.join(
                    current_app.config["UPLOAD_FOLDER"], filename
                )
                files.save(uploaded_file_path)
                p_model = User.get_class_by_tablename(p_table)
                c_model = User.get_class_by_tablename(c_table)
                parent_table = p_model.query.filter_by(id=id).first_or_404()
                if parent_table.images.count() >= 5:
                    return "You cannot add more than 5 images", 400
                new_image = c_model(image_url=filename, job_listing_id=parent_table.id)
                db.session.add(new_image)
                db.session.commit()

                # create thumbnail after saving
                if mime_type.startswith("image"):
                    create_thumbnail(filename)

                # get file size after saving
                size = os.path.getsize(uploaded_file_path)

                # return json for js call back
                result = uploadfile(
                    name=filename, table=c_table, type=mime_type, size=size
                )

            return simplejson.dumps({"files": [result.get_file()]})

    if request.method == "GET":
        # get all file in ./data directory
        p_model = User.get_class_by_tablename(p_table)
        c_model = User.get_class_by_tablename(c_table)
        parent_table = p_model.query.filter_by(id=id).first_or_404()
        files = [
            f.image_url
            for f in c_model.query.filter_by(job_listing_id=parent_table.id).all()
            if os.path.isfile(
                os.path.join(current_app.config["UPLOAD_FOLDER"], f.image_url)
            )
        ]
        file_display = []

        for f in files:
            size = os.path.getsize(os.path.join(current_app.config["UPLOAD_FOLDER"], f))
            file_saved = uploadfile(name=f, size=size, table=c_table)
            file_display.append(file_saved.get_file())

        return simplejson.dumps({"files": file_display})

    return redirect(url_for("index"))


@sender.route("/delete_image/<table>/<string:filename>", methods=["DELETE"])
def delete_image(table, filename):
    file_path = os.path.join(current_app.config["UPLOAD_FOLDER"], filename)
    file_thumb_path = os.path.join(current_app.config["THUMBNAIL_FOLDER"], filename)
    c_model = User.get_class_by_tablename(table)
    table = c_model.query.filter_by(image_url=filename).first_or_404()
    db.session.delete(table)
    db.session.commit()
    if os.path.exists(file_path):
        try:
            os.remove(file_path)

            if os.path.exists(file_thumb_path):
                os.remove(file_thumb_path)

            return simplejson.dumps({filename: "True"})
        except:
            return simplejson.dumps({filename: "False"})


# serve static files
@sender.route("/thumbnail/<string:filename>", methods=["GET"])
def get_thumbnail(filename):
    return send_from_directory(
        current_app.config["THUMBNAIL_FOLDER"], filename=filename
    )


@sender.route("/data/<string:filename>", methods=["GET"])
def get_file(filename):
    return send_from_directory(
        os.path.join(current_app.config["UPLOAD_FOLDER"]), filename=filename
    )

